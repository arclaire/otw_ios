//
//  ViewLeftmenu.swift
//  CLocator
//
//  Created by Lucy on 11/16/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class ViewLeftmenu: UIView {

  private var vwLeftMenu: UIView!
  
  
  //MARK: Outlets
  
  @IBOutlet weak var tblMenu: UITableView!
  
  @IBOutlet weak private var vwHeader: ViewHeaderProfile!
  //MARK: Attributes
  //weak var delHomeView: DelegateViewHome?
  
  @IBOutlet weak var consVwHeaderTopToTopSuperview: NSLayoutConstraint!
  var arrayOfMenu: NSArray = NSArray()
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwLeftMenu = self.loadViewFromNIB()
    if let _ = self.vwLeftMenu {
      self.addSubview(self.vwLeftMenu)
    }
  }
  
  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    self.tblMenu.delegate = self
    self.tblMenu.dataSource = self
    
    self.tblMenu.register(UINib(nibName:"CellMenuLeft", bundle: nil), forCellReuseIdentifier: "CellMenuLeft")
    self.prepareUI()
    
    var rect: CGRect = self.vwHeader.frame
    rect.origin.y = -180//168 //148
    rect.size.height = 180//128
    rect.size.width = UIScreen.main.bounds.size.width
    
    self.vwHeader.frame = rect
    self.consVwHeaderTopToTopSuperview.isActive = false
    self.vwHeader.translatesAutoresizingMaskIntoConstraints = true
    //    self.setUIPosition(isLeft: self.isLeft)
    self.tblMenu.addSubview(self.vwHeader)
    self.tblMenu.contentInset = UIEdgeInsetsMake(180, 0, 0, 0) //108
    
    self.setData()
    
    self.tblMenu.separatorColor = UIColor.clear
  }
  
  //MARK: Data Methods
  func setData() {
    let dictionary1 = ["title": TXT_DASHBOARD.capitalized, "img": "icon_menu_dashboard"]
    let dictionary2 = ["title": TXT_BASE_LOCATION.capitalized,"img": "icon_menu_baseLocation"]
    let dictionary3 = ["title": TXT_FRIENDS, "img": "icon_menu_friends"]
    let dictionary4 = ["title": TXT_SETTINGS, "img": "icon_menu_settings"]
    
    self.arrayOfMenu = [dictionary1, dictionary2, dictionary3, dictionary4]
    
    self.tblMenu.reloadData()
  }
  
  func prepareUI() {
    self.vwLeftMenu.backgroundColor = COLOR_BG_DEFAULT
    self.tblMenu.backgroundColor = UIColor.clear
  }
  //MARK: ButtonAction

}

extension ViewLeftmenu: UITableViewDelegate {
  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 44.0
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 44.0
  }
  
}

extension ViewLeftmenu: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.arrayOfMenu.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CellMenuLeft", for: indexPath) as! CellMenuLeft
    if self.arrayOfMenu.count > 0 {
      
      cell.setData(data: self.arrayOfMenu[indexPath.row] as! NSDictionary)
    }
    
    return cell
  }
  
  
}
