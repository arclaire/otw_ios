//
//  CellMenuLeft.swift
//  CLocator
//
//  Created by Lucy on 12/5/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class CellMenuLeft: UITableViewCell {

  @IBOutlet weak var imgIcon: UIImageView!
  @IBOutlet weak var lblTitle: UILabel!
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
   
    self.prepareUI()
      // Initialization code
  }

  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

      // Configure the view for the selected state
  }
  
  func prepareUI() {
    self.backgroundColor = UIColor.clear
    self.lblTitle.font = FONT_UBUNTU_TXT_LIGHT
    self.lblTitle.textColor = COLOR_TEXT_GRAY
  }

  //MARK: - Data Methods

  func setData(data: NSDictionary) {
    
    if let strTitle: String = data.object(forKey: "title") as? String {
      self.lblTitle.text = strTitle
    }
    
    if let strImg: String =  data.object(forKey: "img") as? String {
      self.imgIcon.image = UIImage(named: strImg)
    }
  }
  
}

