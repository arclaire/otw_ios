//
//  WfLeftmenu.swift
//  CLocator
//
//  Created by Lucy on 11/16/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class WfLeftmenu: NSObject {

  var vcHome: VCHome?
  var vcRootLeft: VCLeftmenu?
  
  var vcRoot: VCRoot?
  
  func addLeftPanelViewController() {
    
    if (self.vcRootLeft == nil) {
      let sbMain = UIStoryboard.getMainStoryboard()
      self.vcRootLeft = (sbMain.instantiateViewController(withIdentifier: ID_VC_LEFT_MENU) as? VCLeftmenu)!
      self.addChildLeftVC(vcLeft: self.vcRootLeft!)
    }
  }
  
  func addChildLeftVC(vcLeft: VCLeftmenu) {
    
    if self.vcRootLeft != nil {
      self.vcRoot?.view.insertSubview(vcLeft.view, at: 0)
      
      self.vcRoot?.addChildViewController(vcLeft)
      self.vcRootLeft?.didMove(toParentViewController: self.vcRoot)
    }
  }
}
