//
//  ViewSignUpNumber.swift
//  CLocator
//
//  Created by Lucy on 12/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

protocol DelegateViewSignUpNumber: NSObjectProtocol {
  func delegateSignupNumber(strNumber: String)
 
  
}


class ViewSignUpNumber: UIView {

  private var vwSignUpNumber: UIView!
  
  //MARK: Outlets
  @IBOutlet weak private var imgBg: UIImageView!
  
  @IBOutlet weak private var btnSignup: UIButton!
  @IBOutlet weak private var btnPhoneCode: UIButton!
  
  @IBOutlet weak private var lblInfo: UILabel!
  
  @IBOutlet weak private var vwPhone: UIView!
  
  @IBOutlet weak private var txtPhoneNumber: UITextFieldWithInset!
  
  //MARK: Layout
  
  //MARK: Attributes
  weak var delSignUpNumber: DelegateViewSignUpNumber?
  
  
  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwSignUpNumber = self.loadViewFromNIB()
    if let _ = self.vwSignUpNumber {
      self.addSubview(self.vwSignUpNumber)
    }
  }
  
  //MARK: - Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    self.prepareUI()
  }
  
  //MARK: - UI Methods
  
  func prepareUI() {
    
    self.btnSignup.layer.cornerRadius = 20
    self.btnSignup.titleLabel?.font = FONT_UBUNTU_BUTTON_BOLD
    self.btnSignup.setTitle(TXT_SIGN_UP, for: UIControlState.normal)
    self.btnSignup.backgroundColor = COLOR_GREEN_DEFAULT
    self.btnSignup.setTitleColor(UIColor.white, for: UIControlState.normal)
    
    self.btnPhoneCode.backgroundColor = UIColor.white
    self.btnPhoneCode.layer.borderColor = UIColor.lightGray.cgColor
    self.btnPhoneCode.layer.borderWidth = 0.5
   
    self.txtPhoneNumber.keyboardType = UIKeyboardType.numberPad
    self.txtPhoneNumber.returnKeyType = UIReturnKeyType.done
    self.txtPhoneNumber.backgroundColor = UIColor.clear
    
    self.txtPhoneNumber.insetTextfield = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
    
    self.vwSignUpNumber.backgroundColor = COLOR_BG_DEFAULT
    self.vwSignUpNumber.clipsToBounds = true
    self.vwPhone.backgroundColor = UIColor.white
    
    self.txtPhoneNumber.placeholder = TXT_PHONE_NUMBER
    
    self.lblInfo.font = FONT_UBUNTU_TXT_LIGHT
    self.lblInfo.text = TXT_INFO_TITLE_SIGN_UP_PHONE
    self.lblInfo.backgroundColor = UIColor.clear
    self.lblInfo.textColor = COLOR_TEXT_BLUE
  
  }
  
  //MARK: - Data Methods
  
  
  //MARK: - ButtonAction
  @IBAction func doTapButton(_ sender: Any) {
    
    if let btn: UIButton = sender as? UIButton {
      if btn == self.btnSignup {
        self.doSignupNumber()
      }
    }
  }
  
  func doSignupNumber() {
    if let strNumber: String = self.txtPhoneNumber.text {
      if strNumber.characters.count > 0 {
        if let strCode: String = self.btnPhoneCode.titleLabel?.text {
          if !strCode.isEmpty {
            self.endEditing(true)
            self.delSignUpNumber?.delegateSignupNumber(strNumber: strNumber)
          }
        }
      }
    }
    
  }
}
