//
//  VCSignUpNumber.swift
//  CLocator
//
//  Created by Lucy on 12/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class VCSignUpNumber: UIViewController {

  weak var eventHandler: MISignUpNumber?
  
  @IBOutlet var vwSignUpNumber: ViewSignUpNumber!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.navigationController?.navigationBar.isHidden = false
    
    
    self.vwSignUpNumber.delSignUpNumber = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if let nav: RootNB = self.navigationController?.navigationBar as? RootNB {
      nav.prepareUINavBackWithTitle(str: TXT_SIGN_UP)
      nav.delegateNB = self
      nav.nvcParent = self.navigationController
      
    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
  }
  
}

extension VCSignUpNumber: DelegateViewSignUpNumber {
  
  func delegateSignupNumber(strNumber: String) {
    self.eventHandler?.doSignupNumber(strNumber: strNumber)
  }
}

extension VCSignUpNumber: DelegateRootNB {
  
  func delegateToggleLeft() {
    self.eventHandler?.doTapBack()
  }
  
  func delegateToggleRight() {
    
  }
  
}
