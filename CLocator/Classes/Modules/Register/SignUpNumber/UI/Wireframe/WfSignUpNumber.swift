//
//  WfSignUpNumber.swift
//  CLocator
//
//  Created by Lucy on 12/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class WfSignUpNumber: NSObject {

  var vcSignUpNumber: VCSignUpNumber?
  var vcRoot: VCRoot?
  
  var vcHome: VCHome?
  var pSignUpNumber: PresenterSignUpNumber?
  
  var wfVerification: WfVerification?
  
  var nvc: UINavigationController?
   
  func pushSignUpNumberVCFromVc() {
    
    self.vcSignUpNumber = self.vcSignUpNumberFromStoryboard()
    
    let iSignUpNumber: ISignUpNumber = ISignUpNumber()
    self.pSignUpNumber = PresenterSignUpNumber()
    self.pSignUpNumber?.wfSignUpNumber = self
    self.pSignUpNumber?.iSignUpNumber = iSignUpNumber
    self.pSignUpNumber?.iInputSignUpNumber = iSignUpNumber
    self.pSignUpNumber?.iSignUpNumber?.iOuputSignUpNumber = pSignUpNumber
    
    self.vcSignUpNumber?.eventHandler = self.pSignUpNumber
    self.nvc?.pushViewController(self.vcSignUpNumber!, animated: true)
    
  }
  
  func pushVerificationVC() {
    self.wfVerification = WfVerification()
    self.wfVerification?.nvc = self.nvc
    self.wfVerification?.pushSignUpNumberVCFromVc()
  }
  
  func vcSignUpNumberFromStoryboard() -> VCSignUpNumber {
    let storyboard = UIStoryboard.getMainStoryboard()
    let viewController = storyboard.instantiateViewController(withIdentifier: ID_VC_SIGN_UP_NUMBER) as! VCSignUpNumber
    return viewController
  }
  
  func popSignUpNumberVC() {
    if let nvc: UINavigationController = self.nvc {
      if let nvcBar: RootNB = nvc.navigationBar as? RootNB {
        nvcBar.isHidden = true
      }
      nvc.popViewController(animated: true)
    }
  }
}



