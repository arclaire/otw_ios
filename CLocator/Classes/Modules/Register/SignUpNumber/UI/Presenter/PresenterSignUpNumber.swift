//
//  PresenterSignUpNumber.swift
//  CLocator
//
//  Created by Lucy on 12/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class PresenterSignUpNumber: NSObject {

  var wfSignUpNumber: WfSignUpNumber?
  var iSignUpNumber: ISignUpNumber?
  weak var iInputSignUpNumber: IInputSignUpNumber?

}

extension PresenterSignUpNumber: MISignUpNumber {
  
  func doTapBack() {
    
    self.iInputSignUpNumber?.doTapBack()
  }

  func doSignupNumber(strNumber: String) {
    
    self.iInputSignUpNumber?.doSignUpWithNumber(strNumber: strNumber)
  }
}

extension PresenterSignUpNumber: IOutputSignUpNumber {
  func goBack() {
    self.wfSignUpNumber?.popSignUpNumberVC()
  }
  
  func goToVerification() {
    self.wfSignUpNumber?.pushVerificationVC()
  }
}
