//
//  IInputSignUpNumber.swift
//  CLocator
//
//  Created by Lucy on 12/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import Foundation

protocol IInputSignUpNumber: NSObjectProtocol {
  
  func doTapBack()
  func doSignUpWithNumber(strNumber: String)
}

