//
//  ISignUpNumber.swift
//  CLocator
//
//  Created by Lucy on 12/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class ISignUpNumber: NSObject {

  weak var iInputSignUpNumber: IInputSignUpNumber?
  weak var iOuputSignUpNumber: IOutputSignUpNumber?
}



extension ISignUpNumber: IInputSignUpNumber {
  
  func doTapBack() {
    self.iOuputSignUpNumber?.goBack()
  }
  
  func doSignUpWithNumber(strNumber: String) {
    debugPrint("SIGN UP WITH NUMBER", strNumber)
    self.iOuputSignUpNumber?.goToVerification()
  }
}
