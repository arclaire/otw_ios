//
//  MISignUpNumber.swift
//  CLocator
//
//  Created by Lucy on 12/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import Foundation
import UIKit

protocol MISignUpNumber: NSObjectProtocol {
  func doTapBack()
  func doSignupNumber(strNumber: String)
}
