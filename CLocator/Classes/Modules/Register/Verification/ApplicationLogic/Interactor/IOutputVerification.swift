//
//  IOutputVerification.swift
//  CLocator
//
//  Created by Lucy on 12/28/16.
//  Copyright © 2016 locator. All rights reserved.
//

import Foundation

protocol IOutputVerification: NSObjectProtocol {
  func goBack()
  func goToEditProfile()
  
}
