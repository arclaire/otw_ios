//
//  IVerification.swift
//  CLocator
//
//  Created by Lucy on 12/28/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class IVerification: NSObject {
  weak var iInputVerification: IInputVerification?
  weak var iOuputVerification: IOutputVerification?
}



extension IVerification: IInputVerification {
  func doTapBack() {
   self.iOuputVerification?.goBack()
  }
  
  func doSendCode(strCode: String) {
    debugPrint("CODE", strCode)
    self.iOuputVerification?.goToEditProfile()
  }
}


 
