//
//  MIVerification.swift
//  CLocator
//
//  Created by Lucy on 12/28/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

protocol MIVerification: NSObjectProtocol {
  func doGoBackFrom()
  func doTapDoneSubmitCodeFrom(strCode: String)
}
