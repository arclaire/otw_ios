//
//  WfVerification.swift
//  CLocator
//
//  Created by Lucy on 12/28/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class WfVerification: NSObject {

  var vcVerification: VCVerification?
  
  var vcRoot: VCRoot?
  
  var nvc: UINavigationController?
 
  var pVerification: PresenterVerification?
  
  var wfProfile: WfProfile?

  func pushSignUpNumberVCFromVc() {
    
    self.vcVerification = self.vcVerificationFromStoryboard()
    self.pVerification = PresenterVerification()
    let iVerification: IVerification = IVerification()
    
    self.pVerification?.wfVerification = self
    self.pVerification?.iVerification = iVerification
    self.pVerification?.iInputVerification = iVerification
    self.pVerification?.iVerification?.iOuputVerification = pVerification
    
    self.vcVerification?.eventHandler = self.pVerification
    
    if let nvc: UINavigationController = self.nvc {
      nvc.pushViewController(self.vcVerification!, animated: true)
    }
  }
  
  func vcVerificationFromStoryboard() -> VCVerification {
    let storyboard = UIStoryboard.getMainStoryboard()
    let viewController = storyboard.instantiateViewController(withIdentifier: ID_VC_VERIFICATION) as! VCVerification
    return viewController
  }
  
  func popVerificationVC() {
    if let nvc: UINavigationController = self.nvc {
      nvc.popViewController(animated: true)
    }
  }
  
  func pushProfileVC(){
    self.wfProfile = WfProfile()
    self.wfProfile?.nvc = self.nvc
    self.wfProfile?.pushProfileVCFromVc()
  }
}
