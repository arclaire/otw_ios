//
//  VCVerification.swift
//  CLocator
//
//  Created by Lucy on 12/28/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class VCVerification: UIViewController {

  weak var eventHandler: MIVerification?
  
  @IBOutlet var vwVerification: ViewVerification!
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    self.vwVerification.delVerification = self
    
  }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if let nav: RootNB = self.navigationController?.navigationBar as? RootNB {
      nav.prepareUINavBackWithTitle(str: TXT_SIGN_UP)
      nav.delegateNB = self
      nav.nvcParent = self.navigationController
      
    }
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.vwVerification.showKeyboard()
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
  }
}

extension VCVerification: DelegateViewVerification {
  func delegateDoneInputCode(strCode: String) {
   self.eventHandler?.doTapDoneSubmitCodeFrom(strCode: strCode)
  }
}
extension VCVerification: DelegateRootNB {
  
  func delegateToggleLeft() {
    self.eventHandler?.doGoBackFrom()
  }
  
  func delegateToggleRight() {
    
  }
  
}
