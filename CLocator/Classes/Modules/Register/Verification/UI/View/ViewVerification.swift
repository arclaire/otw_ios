//
//  ViewVerification.swift
//  CLocator
//
//  Created by Lucy on 12/28/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

protocol DelegateViewVerification: NSObjectProtocol {
  func delegateDoneInputCode(strCode: String)
  
  
}


class ViewVerification: UIView {

  private var vwVerification: UIView!
  @IBOutlet weak var lblInfo: UILabel!
  
  @IBOutlet weak var vwPhone: UIView!
  
  private var vwAccKeyboard: ViewAccessoriesKeyboard?
  
  @IBOutlet weak private var vwTxt1: UIView!
  @IBOutlet weak private var vwTxt2: UIView!
  @IBOutlet weak private var vwTxt3: UIView!
  @IBOutlet weak private var vwTxt4: UIView!
  
  @IBOutlet weak private var vwLine1: UIView!
  @IBOutlet weak private var vwLine2: UIView!
  @IBOutlet weak private var vwLine3: UIView!
  @IBOutlet weak private var vwLine4: UIView!
  
  @IBOutlet weak var txt1: UITextField!
  @IBOutlet weak var txt2: UITextField!
  @IBOutlet weak var txt3: UITextField!
  @IBOutlet weak var txt4: UITextField!
  
  @IBOutlet weak var imgBg: UIImageView!
  
  @IBOutlet weak var btnSend: UIButton!
  
  //MARK: Layouts
  
  @IBOutlet weak var consLine1Height: NSLayoutConstraint!
  @IBOutlet weak var consLine2Height: NSLayoutConstraint!
  @IBOutlet weak var consLine3Height: NSLayoutConstraint!
  @IBOutlet weak var consLine4Height: NSLayoutConstraint!
  
  
  //MARK: Attributes
  weak var delVerification : DelegateViewVerification?
  
  
  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwVerification = self.loadViewFromNIB()
    if let _ = self.vwVerification {
      self.addSubview(self.vwVerification)
    }
  }
  
  //MARK: - Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    self.prepareUI()
  }
  
  //MARK: - UI Methods
  
  func prepareUI() {
    
    self.vwTxt1.backgroundColor = UIColor.clear
    self.vwTxt2.backgroundColor = UIColor.clear
    self.vwTxt3.backgroundColor = UIColor.clear
    self.vwTxt4.backgroundColor = UIColor.clear
    
    self.vwLine1.backgroundColor = UIColor.lightGray
    self.vwLine2.backgroundColor = UIColor.lightGray
    self.vwLine3.backgroundColor = UIColor.lightGray
    self.vwLine4.backgroundColor = UIColor.lightGray
    
    self.txt1.keyboardType = UIKeyboardType.numberPad
    self.txt1.returnKeyType = UIReturnKeyType.next
    self.txt1.backgroundColor = UIColor.clear
    self.txt1.placeholder = ""
    self.txt1.text = ""
    self.txt1.delegate = self
    
    self.txt2.keyboardType = UIKeyboardType.numberPad
    self.txt2.returnKeyType = UIReturnKeyType.next
    self.txt2.backgroundColor = UIColor.clear
    self.txt2.placeholder = ""
    self.txt2.text = ""
    self.txt2.delegate = self
    
    self.txt3.keyboardType = UIKeyboardType.numberPad
    self.txt3.returnKeyType = UIReturnKeyType.next
    self.txt3.backgroundColor = UIColor.clear
    self.txt3.placeholder = ""
    self.txt3.text = ""
    self.txt3.delegate = self
    
    self.txt4.keyboardType = UIKeyboardType.numberPad
    self.txt4.returnKeyType = UIReturnKeyType.next
    self.txt4.backgroundColor = UIColor.clear
    self.txt4.placeholder = ""
    self.txt4.text = ""
    self.txt4.delegate = self
    
    self.lblInfo.font = FONT_UBUNTU_TXT_LIGHT
    self.lblInfo.text = TXT_INFO_TITLE_SIGN_UP_PHONE
    self.lblInfo.backgroundColor = UIColor.clear
    self.lblInfo.textColor = COLOR_TEXT_BLUE
    
    self.btnSend.titleLabel?.font = FONT_UBUNTU_TXT_LIGHT
    self.btnSend.setTitle(TXT_SEND_CODE_AGAIN, for: UIControlState.normal)
    self.btnSend.setTitleColor(COLOR_GREEN_DEFAULT, for: UIControlState.normal)
    
    self.vwVerification.backgroundColor = COLOR_BG_DEFAULT
    self.vwVerification.clipsToBounds = true

    self.vwPhone.backgroundColor = UIColor.clear
    
    self.vwAccKeyboard = ViewAccessoriesKeyboard.getViewFromNib(bundle: nil, nibName: ID_NIB_VW_CUSTOM, tag: ID_TAG_VW_ACCKEYBOARD) as? ViewAccessoriesKeyboard
    self.vwAccKeyboard?.delViewAccKeyboard = self
    self.txt4.inputAccessoryView = self.vwAccKeyboard
    self.txt3.inputAccessoryView = nil
    self.txt2.inputAccessoryView = nil
    self.txt1.inputAccessoryView = nil
  }

  func showKeyboard() {
    self.txt1.becomeFirstResponder()
  }
  
  func switcActiveTextfield(textField: UITextField) {
    if textField == self.txt1 {
      self.txt2.becomeFirstResponder()
      
    } else if textField == self.txt2 {
      self.txt3.becomeFirstResponder()
      
    } else if textField == self.txt3 {
      self.txt4.becomeFirstResponder()
    }
  }
  
  func switcActiveTextfieldDelete(textField: UITextField) {
    if textField == self.txt4 {
      self.txt3.becomeFirstResponder()
      
    } else if textField == self.txt3 {
      self.txt2.becomeFirstResponder()
      
    } else if textField == self.txt2 {
      self.txt1.becomeFirstResponder()
    }
  }
  
  //MARK: - Actions
  @IBAction func doTappedButton(_ sender: Any) {
  }
  
  @IBAction func textEditingChanged(_ sender: Any) {
    
    if let txt: UITextField = sender as? UITextField {
      if txt.text?.characters.count == 1 {
        self.switcActiveTextfield(textField: txt)
      } else if txt.text?.characters.count == 0 {
        txt.text = ""
        self.switcActiveTextfieldDelete(textField: txt)
      }
    }
  }
  
}

extension ViewVerification: DelegateViewAccessoriesKeyboard {
  func delegateTapBtnDone() {
    if !(self.txt1.text?.isEmpty)! && !(self.txt2.text?.isEmpty)! && !(self.txt3.text?.isEmpty)! && !(self.txt4.text?.isEmpty)! {
      let strCode: String = String(format:"%@%@%@%@", self.txt1.text!, self.txt2.text!, self.txt3.text!, self.txt4.text!)
      self.delVerification?.delegateDoneInputCode(strCode: strCode)
    }
  }
}
extension ViewVerification: UITextFieldDelegate {
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if (textField.text!.characters.count + string.characters.count - range.length) > 1 {
      
      return false
    } else {
      return true
    }
  }
  
}
