//
//  PresenterVerification.swift
//  CLocator
//
//  Created by Lucy on 12/28/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class PresenterVerification: NSObject {

  var wfVerification: WfVerification?
  var iVerification: IVerification?
  weak var iInputVerification: IInputVerification?
}

extension PresenterVerification: MIVerification {
  
  func doGoBackFrom() {
    self.iInputVerification?.doTapBack()
  }
  
  func doTapDoneSubmitCodeFrom(strCode: String) {
    self.iInputVerification?.doSendCode(strCode: strCode)
  }
}

extension PresenterVerification: IOutputVerification {
  func goToEditProfile() {
    self.wfVerification?.pushProfileVC()
  }
  
  func goBack() {
    
    self.wfVerification?.popVerificationVC()
  }
}
