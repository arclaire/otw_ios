//
//  MIForgetPass.swift
//  CLocator
//
//  Created by Lucy on 12/10/16.
//  Copyright © 2016 locator. All rights reserved.
//

import Foundation
import UIKit

protocol MIForgotPass: NSObjectProtocol {
  
  func doTapBack()
  func doTapSendEmail(strEmail: String)
  
}
