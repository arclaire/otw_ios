//
//  IOutputForgotPass.swift
//  CLocator
//
//  Created by Lucy on 12/8/16.
//  Copyright © 2016 locator. All rights reserved.
//

import Foundation

protocol IOutputForgotPass: NSObjectProtocol {
  
  func goBack()
  func showAlertSent()
}
