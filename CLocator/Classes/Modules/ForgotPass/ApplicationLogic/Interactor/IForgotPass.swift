//
//  IForgotPass.swift
//  CLocator
//
//  Created by Lucy on 12/8/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class IForgotPass: NSObject {
  weak var iInputForgotPass: IInputForgotPass?
  weak var iOuputForgotPass: IOutputForgotPass?
  
}

extension IForgotPass: IInputForgotPass {
  func doTapBack() {
    self.iOuputForgotPass?.goBack()
  }
  
  func doTapSendEmail(strEmail: String) {
    debugPrint("Email", strEmail)
    self.iOuputForgotPass?.showAlertSent()
  }
}
