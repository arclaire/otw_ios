//
//  VCForgotPass.swift
//  CLocator
//
//  Created by Lucy on 12/7/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class VCForgotPass: UIViewController {

  weak var eventHandler: MIForgotPass?
  
  @IBOutlet var vwForgotPass: ViewForgotPass!
  override func viewDidLoad() {

    super.viewDidLoad()
    self.navigationController?.navigationBar.isHidden = false
    self.vwForgotPass.delViewForgotPass = self
    
    if let nav: RootNB = self.navigationController?.navigationBar as? RootNB {
      nav.prepareUINavBackWithTitle(str: TXT_FORGOT_PASSWORD)
      nav.delegateNB = self
      nav.nvcParent = self.navigationController
      
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    
    super.viewWillDisappear(animated)
    
  }

  override func viewDidDisappear(_ animated: Bool) {
    
    super.viewDidDisappear(animated)
    
  }
  
  override func didReceiveMemoryWarning() {
    
    super.didReceiveMemoryWarning()

  }
  
}

extension VCForgotPass: DelegateViewForgotPass {
  func delegateDoSentEmail(strEmail: String) {
    self.eventHandler?.doTapSendEmail(strEmail: strEmail)
  }
}

extension VCForgotPass: DelegateRootNB {

  func delegateToggleLeft() {
    self.eventHandler?.doTapBack()
  }
  
  func delegateToggleRight() {
    
  }

}
