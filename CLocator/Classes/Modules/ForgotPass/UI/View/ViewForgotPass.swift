//
//  ViewForgotPass.swift
//  CLocator
//
//  Created by Lucy on 12/7/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

protocol DelegateViewForgotPass: NSObjectProtocol {
  func delegateDoSentEmail(strEmail: String)
  
}


class ViewForgotPass: UIView {

  //MARK: Outlets
  
  private var vwForgotPass: UIView!
  
  @IBOutlet weak private var lblInfo: UILabel!
  @IBOutlet weak private var txtEmail: UITextFieldWithInset!
  @IBOutlet weak private var btnSend: UIButton!
  @IBOutlet weak private var vwEmail: UIView!
  
  weak var delViewForgotPass: DelegateViewForgotPass?
  //MARK: Attributes
  
//  weak var delHomeView: DelegateViewHome?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwForgotPass = self.loadViewFromNIB()
    if let _ = self.vwForgotPass {
      self.addSubview(self.vwForgotPass)
    }
  }
  
  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
   
    self.prepareUI()
  }
  
  //MARK: Data Methods
  func setData() {
    
  }
  
  func prepareUI() {
    
    self.btnSend.layer.cornerRadius = 20
    self.btnSend.titleLabel?.font = FONT_UBUNTU_BUTTON_BOLD
    self.btnSend.setTitle(TXT_SEND, for: UIControlState.normal)
    self.btnSend.backgroundColor = COLOR_GREEN_DEFAULT
    self.btnSend.setTitleColor(UIColor.white, for: UIControlState.normal)
    
    self.txtEmail.keyboardType = UIKeyboardType.emailAddress
    self.txtEmail.returnKeyType = UIReturnKeyType.done
    self.txtEmail.backgroundColor = UIColor.clear
    
    self.txtEmail.insetTextfield = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
  
    self.vwForgotPass.backgroundColor = COLOR_BG_DEFAULT
    self.vwEmail.backgroundColor = UIColor.white
    
    self.txtEmail.placeholder = TXT_EMAIL
    
    self.lblInfo.font = FONT_UBUNTU_TXT_LIGHT
    self.lblInfo.text = TXT_INFO_TITLE_FORGOT_PASSWORD
    self.lblInfo.backgroundColor = UIColor.clear
    self.lblInfo.textColor = COLOR_TEXT_BLUE
   
  }
  
  //MARK: ButtonAction
  
  @IBAction func doTapButton(_ sender: Any) {
    
    if let btn: UIButton = sender as? UIButton {
      if btn == self.btnSend {
        self.validateEmail()
      }
    }
  }
  
  func validateEmail() {
    if let strEmail: String = self.txtEmail.text {
      if strEmail.characters.count > 0 {
         self.delViewForgotPass?.delegateDoSentEmail(strEmail: strEmail)
      }
    }
  }
}
