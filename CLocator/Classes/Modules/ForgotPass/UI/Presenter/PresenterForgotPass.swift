//
//  PresenterForgotPass.swift
//  CLocator
//
//  Created by Lucy on 12/10/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class PresenterForgotPass: NSObject {

  var wfForgotPass: WfForgotPass?
  var iForgotPass: IForgotPass?
  
  weak var iInputForgotPass: IInputForgotPass?
  
}

extension PresenterForgotPass: MIForgotPass {
  func doTapSendEmail(strEmail: String) {
    self.iInputForgotPass?.doTapSendEmail(strEmail: strEmail)
  }
  
  func doTapBack() {
    self.iInputForgotPass?.doTapBack()
  }
}

extension PresenterForgotPass: IOutputForgotPass {
  func goBack() {
    self.wfForgotPass?.popForgotPassVC()
  }
  
  func showAlertSent() {
    
  }
}
