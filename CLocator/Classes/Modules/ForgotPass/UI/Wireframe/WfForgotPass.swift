//
//  WfForgotPass.swift
//  CLocator
//
//  Created by Lucy on 12/7/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class WfForgotPass: NSObject {
  var vcForgotPass: VCForgotPass?
  var pForgotPass: PresenterForgotPass?
  
  var nvc: UINavigationController?
  
  func pushForgotPasVCFromVc() {
    
    self.vcForgotPass = self.vcForgotPassFromStoryboard()
    let iForgotPass: IForgotPass = IForgotPass()
   
    self.pForgotPass = PresenterForgotPass()
    
    self.pForgotPass?.wfForgotPass = self
    self.pForgotPass?.iForgotPass = iForgotPass
    self.pForgotPass?.iInputForgotPass = iForgotPass
    self.pForgotPass?.iForgotPass?.iOuputForgotPass = pForgotPass

    self.vcForgotPass?.eventHandler = self.pForgotPass
    self.nvc?.pushViewController(self.vcForgotPass!, animated: true)
   
  }
  
  func vcForgotPassFromStoryboard() -> VCForgotPass {
    let storyboard = UIStoryboard.getMainStoryboard()
    let viewController = storyboard.instantiateViewController(withIdentifier: ID_VC_FORGOT_PASS) as! VCForgotPass
    return viewController
  }
  
  func popForgotPassVC() {
    if let nvc: UINavigationController = self.nvc {
      nvc.popViewController(animated: true)
      if let nvcBar: RootNB = nvc.navigationBar as? RootNB {
        nvcBar.isHidden = true
      }
    }
  }
}
