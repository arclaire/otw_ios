//
//  IProfile.swift
//  CLocator
//
//  Created by Lucy on 12/31/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class IProfile: NSObject {
  weak var iInputProfile: IInputProfile?
  weak var iOutputProfile: IOutputProfile?
}



extension IProfile: IInputProfile {
  func doTapBack() {
    self.iOutputProfile?.goBack()
  }
  
  func doSave(img: UIImage, dictData: NSDictionary) {
    
  }
}

