//
//  IOutputProfile.swift
//  CLocator
//
//  Created by Lucy on 12/31/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit
import Foundation

protocol IOutputProfile: NSObjectProtocol {
  func goBack()
  func goToConnectFriends()
  func dismissProfile()
}
