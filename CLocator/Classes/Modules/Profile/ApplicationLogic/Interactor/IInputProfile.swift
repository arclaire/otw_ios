//
//  IInputProfile.swift
//  CLocator
//
//  Created by Lucy on 12/31/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

import Foundation

protocol IInputProfile: NSObjectProtocol {
  func doTapBack()
  func doSave(img: UIImage, dictData:NSDictionary)
}
