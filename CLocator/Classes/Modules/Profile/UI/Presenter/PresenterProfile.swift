//
//  PresenterProfile.swift
//  CLocator
//
//  Created by Lucy on 12/31/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class PresenterProfile: NSObject {
  var wfProfile: WfProfile?
  var iProfile: IProfile?
  weak var iInputProfile: IInputProfile?
}

extension PresenterProfile: MIProfile {
 
  func doSave(img: UIImage, dictData: NSDictionary) {
    self.iInputProfile?.doSave(img: img, dictData: dictData)
  }
  
  func doTapBack() {
    self.iInputProfile?.doTapBack()
  }
}

extension PresenterProfile: IOutputProfile {
  func dismissProfile() {
    
  }
  
  func goToConnectFriends() {
    
  }
  
  func goBack() {
    self.wfProfile?.popProfileVC()
  }
  
}
