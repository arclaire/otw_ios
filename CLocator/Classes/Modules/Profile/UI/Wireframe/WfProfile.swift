//
//  WfProfile.swift
//  CLocator
//
//  Created by Lucy on 12/31/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class WfProfile: NSObject {
    
    var vcProfile: VCProfile?
    
    var vcRoot: VCRoot?
    
    var nvc: UINavigationController?
    
    var pProfile: PresenterProfile?
    
    
    func pushProfileVCFromVc() {
      
      self.vcProfile = self.vcProfileFromStoryboard()
      self.pProfile = PresenterProfile()
      let iProfile: IProfile = IProfile()
      
      self.pProfile?.wfProfile = self
      self.pProfile?.iProfile = iProfile
      self.pProfile?.iInputProfile = iProfile
      self.pProfile?.iProfile?.iOutputProfile = pProfile
      
      self.vcProfile?.eventHandler = self.pProfile
      
      if let nvc: UINavigationController = self.nvc {
        nvc.pushViewController(self.vcProfile!, animated: true)
      }
    }
    
    func vcProfileFromStoryboard() -> VCProfile {
      let storyboard = UIStoryboard.getMainStoryboard()
      let viewController = storyboard.instantiateViewController(withIdentifier: ID_VC_PROFILE) as! VCProfile
      return viewController
    }
    
    func popProfileVC() {
      if let nvc: UINavigationController = self.nvc {
        nvc.popViewController(animated: true)
      }
    }
}


