//
//  VCProfile.swift
//  CLocator
//
//  Created by Lucy on 12/31/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class VCProfile: UIViewController {

  @IBOutlet var vwProfile: ViewProfile!
  
  weak var eventHandler: MIProfile?
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if let nav: RootNB = self.navigationController?.navigationBar as? RootNB {
      nav.prepareUINavBackWithTitle(str: TXT_PROFILE_INFO)
      nav.delegateNB = self
      nav.nvcParent = self.navigationController
      
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    
  }
}

extension VCProfile: DelegateRootNB {
  
  func delegateToggleLeft() {
    self.eventHandler?.doTapBack()
  }
  
  func delegateToggleRight() {
    
  }
  
}

