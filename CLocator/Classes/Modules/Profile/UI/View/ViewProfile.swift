//
//  ViewProfile.swift
//  CLocator
//
//  Created by Lucy on 12/31/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class ViewProfile: UIView {

  var vwProfile: UIView!
  
  //MARK: Outlets
  @IBOutlet weak var imgBg: UIImageView!
  @IBOutlet weak var imgProfile: UIImageView!
  @IBOutlet weak var imgIconPhone: UIImageView!
  
  @IBOutlet weak var scrollview: UIScrollView!
  
  @IBOutlet weak var vwContainerPhone: UIView!
  @IBOutlet weak var vwContainerInput: UIView!
  @IBOutlet weak var vwInputPhone: UIView!
  
  @IBOutlet weak var txtPhone: UITextFieldWithInset!
  @IBOutlet weak var lblName: UILabel!
  
  
  @IBOutlet weak var btnLogin: UIButton!
  
  
  //MARK: Attributes
  weak var delVerification : DelegateViewVerification?
  
  
  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwProfile = self.loadViewFromNIB()
    if let _ = self.vwProfile {
      self.addSubview(self.vwProfile)
    }
  }
  
  //MARK: - Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    self.prepareUI()
  }
  
  //MARK: - UI Methods
  
  func prepareUI() {
    
  }


}
