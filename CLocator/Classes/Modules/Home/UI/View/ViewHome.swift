//
//  ViewHome.swift
//  CLocator
//
//  Created by Lucy on 11/20/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

protocol DelegateViewHome: NSObjectProtocol {
 
  
}

class ViewHome: UIView {
  
  private var vwHome:UIView!
  
  
  //MARK: Outlets
  
  
  //MARK: Attributes
  weak var delHomeView: DelegateViewHome?
  
  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwHome = self.loadViewFromNIB()
    if let _ = self.vwHome {
      self.addSubview(self.vwHome)
    }
  }
  
  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  //MARK: Data Methods
  
  
  //MARK: ButtonAction
  
 
}
