//
//  VCHome.swift
//  CLocator
//
//  Created by Lucy on 11/17/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

enum stateRoot {
  case stateDefault
  case stateLeftExpanded
  case stateRightExpanded
}

class VCHome: UIViewController {

  @IBOutlet var vwHome: ViewHome!
  
  var eventHandler: MIHome?
  var vcRootLeft: VCLeftmenu?
  
  var stateCurrent: stateRoot = .stateDefault {
    didSet {
      let isAddShadow = stateCurrent != .stateDefault
      self.addShadow(isAddShadow: isAddShadow)
    }
  }
  
  var isFlagIsNotExpanded: Bool = true
  let floatCenterOffset: CGFloat = 80

  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    if let navBar: RootNB = self.navigationController?.navigationBar as? RootNB {
      navBar.delegateNB = self
      navBar.prepareUINavDefaultWithTitle(str: TXT_DASHBOARD)
    }
    
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
  }

  //MARK: - UI METHODS
  func addShadow(isAddShadow: Bool) {
    if (isAddShadow) {
      self.navigationController?.view.layer.shadowOpacity = 0.8
    } else {
      self.navigationController?.view.layer.shadowOpacity = 0.0    }
  }
   
  func animateLeftPanel(isExpanded: Bool) {
    if (isExpanded) {
      self.stateCurrent = .stateLeftExpanded
      self.animateCenterPanelXPosition(targetPosition: (self.navigationController?.view.frame.size.width)! - self.floatCenterOffset)
      //debugPrint("Expanded", self.view.frame)
    } else {
      self.animateCenterPanelXPosition(targetPosition: 0) { finished in
        self.stateCurrent = .stateDefault
         //debugPrint("NotExpanded")
        /*if (self.vcRootLeft != nil) {
          self.vcRootLeft!.view.removeFromSuperview()
          self.vcRootLeft = nil;
        }*/
      }
    }
  }
  
  func animateCenterPanelXPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
      self.navigationController?.view.frame.origin.x = targetPosition
    }, completion: completion)
  }
}

extension VCHome: DelegateRootNB {
  
  func delegateToggleRight() {
    
  }
  
  func delegateToggleLeft() {
    let isNotExpanded = (stateCurrent != .stateLeftExpanded)
    
    if isNotExpanded {
      self.eventHandler?.doToggleNavBarLeftMenu()
      
    }
    
    //self.animateLeftPanel(isExpanded: isNotExpanded)
    self.eventHandler?.animateToggler(isExpanded: isNotExpanded)
  }
}
