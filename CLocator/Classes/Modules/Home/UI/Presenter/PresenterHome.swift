//
//  PresenterHome.swift
//  CLocator
//
//  Created by Lucy on 11/26/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class PresenterHome: NSObject {

  var wfHome: WfHome?
  var iHome: IHome?
  var iInputHome: IInputHome?
}


extension PresenterHome: MIHome {
  
  func doToggleNavBarLeftMenu() {
    self.wfHome?.addLeftMenuAsLeftChild()
  }
  
  func animateToggler(isExpanded: Bool) {
    self.wfHome?.animateToggleLeftMenu(isExpanded: isExpanded)
  }
}
