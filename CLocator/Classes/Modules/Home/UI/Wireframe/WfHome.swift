//
//  WfHome.swift
//  CLocator
//
//  Created by Lucy on 11/17/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class WfHome: NSObject {

  var vcHome: VCHome?

  var vcRoot: VCRoot?
  
  var nvcHome: UINavigationController?
  
  var pHome: PresenterHome?
  
  var wfLeftMenus: WfLeftmenu = WfLeftmenu()

 
  func addHomeAsChild() {
   
    let sbMain = UIStoryboard.getMainStoryboard()
    self.nvcHome = (sbMain.instantiateViewController(withIdentifier: ID_NVC_HOME) as! UINavigationController)
    self.nvcHome?.didMove(toParentViewController: self.vcRoot)
    
    if let vc: VCHome = self.nvcHome?.viewControllers.first as? VCHome {
      
      self.pHome = PresenterHome()
      self.pHome?.wfHome = self
      
      self.vcHome = vc
      
      self.vcHome?.eventHandler = self.pHome
    }
    
    self.vcRoot?.addChildViewController(self.nvcHome!)
    self.vcRoot?.view.addSubview((self.nvcHome?.view!)!)
  }
  
  func addLeftMenuAsLeftChild() {
    self.wfLeftMenus.vcRoot = self.vcRoot
    self.wfLeftMenus.vcHome = self.vcHome
    self.wfLeftMenus.addLeftPanelViewController()
    self.vcHome?.vcRootLeft = self.wfLeftMenus.vcRootLeft
  }
  
  func animateToggleLeftMenu(isExpanded: Bool) {
    self.vcHome?.animateLeftPanel(isExpanded: isExpanded)
  }
}
