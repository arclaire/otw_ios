//
//  MIHome.swift
//  CLocator
//
//  Created by Lucy on 11/26/16.
//  Copyright © 2016 locator. All rights reserved.
//

import Foundation

protocol MIHome: NSObjectProtocol {
  
  func doToggleNavBarLeftMenu()
  func animateToggler(isExpanded: Bool)
  
}
