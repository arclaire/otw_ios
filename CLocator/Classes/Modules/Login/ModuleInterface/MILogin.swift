//
//  MILogin.swift
//  CLocator
//
//  Created by Lucy on 11/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

protocol MILogin: NSObjectProtocol {
  
  func doLogin()
  func doSignup()
  func doGoToForgotPass()
  
}
