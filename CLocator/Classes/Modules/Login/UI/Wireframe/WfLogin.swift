//
//  WfLogin.swift
//  CLocator
//
//  Created by Lucy on 11/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class WfLogin: NSObject {

  var vcLogin: VCLogin?
  
  
  var vcRoot: VCRoot?
  
  var vcHome: VCHome?
  var pLogin: PresenterLogin?
  
  var nvcLogin: UINavigationController?
  var nvcRoot: UINavigationController?
  
  var wfForgotPass: WfForgotPass?
  var wfSignUpNumber: WfSignUpNumber?
  
  func presentLogin() {
    
    let sbMain = UIStoryboard.getMainStoryboard()
    self.nvcLogin = (sbMain.instantiateViewController(withIdentifier: ID_NVC_LOGIN) as! UINavigationController)
   
    
    if let vc: VCLogin = self.nvcLogin?.viewControllers.first as? VCLogin {
      
      self.vcLogin = vc
      
      let iLogin: ILogin = ILogin()
      
      self.pLogin = PresenterLogin()

      self.pLogin?.wfLogin = self
      self.pLogin?.iLogin = iLogin
      self.pLogin?.iInputLogin = iLogin
      self.pLogin?.iLogin?.iOutputLogin = pLogin
      
      self.vcLogin?.eventHandler = self.pLogin 
    }
    
    let vcTop = UIApplication.shared.keyWindow?.rootViewController
    
    vcTop?.present(self.nvcLogin!, animated: false, completion: nil)
  }
  
  func pushForgotPassVC() {
    self.wfForgotPass = WfForgotPass()
    self.wfForgotPass?.nvc = self.nvcLogin!
    self.wfForgotPass?.pushForgotPasVCFromVc()
  }

  func pushSignUpNumberVC() {
    self.wfSignUpNumber = WfSignUpNumber()
    self.wfSignUpNumber?.nvc = self.nvcLogin!
    self.wfSignUpNumber?.pushSignUpNumberVCFromVc()
  }
  
  func dismissLogin() {
    self.vcRoot?.isLogin = true
    self.nvcLogin?.dismiss(animated: true, completion: nil)
    
  }
}
