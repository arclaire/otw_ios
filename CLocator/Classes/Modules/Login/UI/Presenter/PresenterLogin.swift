//
//  PresenterLogin.swift
//  CLocator
//
//  Created by Lucy on 11/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class PresenterLogin: NSObject {

  var wfLogin: WfLogin?
  var iLogin: ILogin?
  var iInputLogin: IInputLogin?
  
}

extension PresenterLogin: MILogin {
  
  func doLogin() {
  
    self.iInputLogin?.doTapLogin()
  }
  
  func doSignup() {
   self.iInputLogin?.doTapSignUp()
  }
  
  func doGoToForgotPass() {
    self.iInputLogin?.doTapForgotPass()
  }
}

extension PresenterLogin: IOutputLogin {
  
  func showSignUpNumber() {
    self.wfLogin?.pushSignUpNumberVC()
  }
  func showForgotPass() {
    
    self.wfLogin?.pushForgotPassVC()
  }
  
  func dismissLogin() {
    
    self.wfLogin?.dismissLogin()
  }
}
