//
//  ViewLogin.swift
//  CLocator
//
//  Created by Lucy on 11/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

protocol DelegateViewLogin: NSObjectProtocol {
  func delegateDoLogin()
  func delegateDoSignup()
  func delegateForgotPass()
}

class ViewLogin: UIView {

  private var vwLogin:UIView!
  
  
  //MARK: Outlets
  
  @IBOutlet weak private var imgvwIcon: UIImageView!
  @IBOutlet weak private var imgvwEmail: UIImageView!
  @IBOutlet weak private var imgvwPass: UIImageView!
  
  @IBOutlet weak private var lblTitle: UILabel!
  @IBOutlet weak private var lblForgotPass: UILabel!
  
  @IBOutlet weak var txtEmail: UITextFieldWithInset!
  
  @IBOutlet weak var scrollview: UIScrollView!
  @IBOutlet weak var txtPass: UITextFieldWithInset!
  @IBOutlet private var  vwParent: UIView!
  @IBOutlet weak private var vwContainerInput: UIView!
  @IBOutlet weak private var vwEmail: UIView!
  @IBOutlet weak private var vwPassword: UIView!
  @IBOutlet weak private var vwLinePassTop: UIView!
  
  @IBOutlet weak var btnLogin: UIButton!
  @IBOutlet weak var btnSignup: UIButton!
  
  //MARK: Layout
  @IBOutlet weak var consVwLinePassTopHeight: NSLayoutConstraint!
  @IBOutlet weak var consImgEmailWidth: NSLayoutConstraint!
  @IBOutlet weak var consImgEmailHeight: NSLayoutConstraint!
  
  @IBOutlet weak var consTxtEmailHeight: NSLayoutConstraint!
  @IBOutlet weak var consTxtPassHeight: NSLayoutConstraint!
  @IBOutlet weak var consImgPassWidth: NSLayoutConstraint!
  @IBOutlet weak var consImgPassHeight: NSLayoutConstraint!
  
  @IBOutlet weak var consBtnLoginHeight: NSLayoutConstraint!
  @IBOutlet weak var consBtnSignUpHeight: NSLayoutConstraint!
  
  
  //MARK: Attributes
  weak var delLoginView: DelegateViewLogin?
  
  private var gestureTapForgotPass: UITapGestureRecognizer!
  
  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwLogin = self.loadViewFromNIB()
    if let _ = self.vwLogin {
      self.addSubview(self.vwLogin)
    }
  }
  
  //MARK: - Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    self.prepareUI()
  }
  
  //MARK: - UI Methods
  
  func prepareUI() {
    self.btnSignup.layer.cornerRadius = 20
    self.btnLogin.layer.cornerRadius = 20
    self.scrollview.showsHorizontalScrollIndicator = false
    self.scrollview.showsVerticalScrollIndicator = false
    
    self.vwContainerInput.backgroundColor = UIColor.white
    self.vwEmail.backgroundColor = UIColor.white
    self.vwPassword.backgroundColor = UIColor.white
    self.vwLinePassTop.backgroundColor = UIColor.gray
  
    self.lblTitle.backgroundColor = UIColor.clear
    self.lblForgotPass.backgroundColor = UIColor.clear
    
    self.vwContainerInput.layer.cornerRadius = 10
    self.vwContainerInput.layer.shadowColor = UIColor.gray.cgColor
    self.vwContainerInput.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)

    self.vwContainerInput.layer.shadowRadius = 2
    self.vwContainerInput.layer.shadowOpacity = 0.4
    //self.vwContainerInput.layer.shadowPath = UIBezierPath(rect: self.vwContainerInput.bounds).cgPath
    self.vwContainerInput.layer.shouldRasterize = true
    self.consVwLinePassTopHeight.constant = 0.5
    
    self.vwParent.backgroundColor = COLOR_BG_DEFAULT
    self.txtPass.backgroundColor = UIColor.white
    self.txtEmail.backgroundColor = UIColor.white
    
    self.lblForgotPass.textColor = COLOR_BLUE_DEFAULT
    self.lblTitle.textColor = COLOR_BLUE_DEFAULT
    
    self.btnLogin.backgroundColor = COLOR_GREEN_DEFAULT
    self.btnSignup.backgroundColor = UIColor.white
    
    self.btnLogin.titleLabel?.font = FONT_UBUNTU_BUTTON_BOLD
    self.btnSignup.titleLabel?.font = FONT_UBUNTU_BUTTON_BOLD
    
    self.txtPass.font = FONT_UBUNTU_TXT_LIGHT
    self.txtEmail.font = FONT_UBUNTU_TXT_LIGHT
    self.lblForgotPass.font = FONT_UBUNTU_TXT_CAPTION_LIGHT
    
    self.lblTitle.font = FONT_UBUNTU_TITLE_BOLD
    
    self.txtPass.insetTextfield = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
    self.txtEmail.insetTextfield = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
  
    self.lblTitle.text = TXT_FAMILY
    self.txtEmail.placeholder = TXT_EMAIL
    self.txtPass.placeholder  = TXT_PASSWORD
    self.lblForgotPass.text = String(format: "%@?", TXT_FORGOT_PASSWORD)
    
    self.btnSignup.setTitle(TXT_SIGN_UP, for: UIControlState.normal)
    self.btnLogin.setTitle(TXT_LOGIN, for: UIControlState.normal)
    
    self.txtEmail.keyboardType = UIKeyboardType.emailAddress
    self.txtPass.isSecureTextEntry = true
    self.txtPass.returnKeyType = UIReturnKeyType.done
    self.txtEmail.returnKeyType = UIReturnKeyType.next
    self.txtEmail.delegate = self
    self.txtPass.delegate = self
  
    self.gestureTapForgotPass = UITapGestureRecognizer(target: self, action:#selector(self.gestureTapAction(_:)))
    self.gestureTapForgotPass.numberOfTapsRequired = 1
    self.lblForgotPass.addGestureRecognizer(self.gestureTapForgotPass)
    
    self.lblForgotPass.isUserInteractionEnabled = true
    
    
  }
  
  //MARK: - Data Methods
  
  
  //MARK: - ButtonAction

  @IBAction func doTapButton(_ sender: Any) {
    
    if let button: UIButton = sender as? UIButton {
      if button == self.btnLogin {
       
        self.delLoginView?.delegateDoLogin()
        
      } else if button == self.btnSignup {
        self.delLoginView?.delegateDoSignup()
      }
    }
  }
  
  func gestureTapAction(_ sender: UITapGestureRecognizer) {
    if sender == self.gestureTapForgotPass {
      self.delLoginView?.delegateForgotPass()
      
    }
  }
 }

extension ViewLogin: UITextFieldDelegate {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
   
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == self.txtEmail {
      self.txtPass.becomeFirstResponder()
      
    } else if textField == self.txtPass {
      
      self.txtPass.resignFirstResponder()
      if (self.txtPass.text?.characters.count)! > 0 && (self.txtEmail.text?.characters.count)! > 0 {
        
        //if self.btnSignInEmailUser.userInteractionEnabled {
          //self.delLoginView?.delegateLoginEmail(self.txtUsernameEmail.text!, stringPass: self.txtPassword.text!)
        //} else {
          //self.txtPass.resignFirstResponder()
        //}
      }
    } else {
      
      
    }
    return true
  }

}
