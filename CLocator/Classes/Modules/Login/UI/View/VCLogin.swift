//
//  VCLogin.swift
//  CLocator
//
//  Created by Lucy on 11/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class VCLogin: UIViewController {

  @IBOutlet var vcLogin: ViewLogin!
  
  weak var eventHandler: MILogin?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationController?.navigationBar.isHidden = true
    self.vcLogin.delLoginView = self
    
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}

extension VCLogin: DelegateViewLogin {
  func delegateForgotPass() {
    self.eventHandler?.doGoToForgotPass()
  }
  
  func delegateDoLogin() {
    self.eventHandler?.doLogin()
  }
  
  func delegateDoSignup() {
    self.eventHandler?.doSignup()
  }
 
}
