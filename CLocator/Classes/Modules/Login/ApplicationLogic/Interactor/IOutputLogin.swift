//
//  IOutputLogin.swift
//  CLocator
//
//  Created by Lucy on 11/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import Foundation

protocol IOutputLogin: NSObjectProtocol {
  
  func showForgotPass()
  func dismissLogin()
  func showSignUpNumber()
  
}
