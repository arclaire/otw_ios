//
//  ILogin.swift
//  CLocator
//
//  Created by Lucy on 11/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class ILogin: NSObject {

  weak var iInputLogin: IInputLogin?
  weak var iOutputLogin: IOutputLogin?

}

extension ILogin: IInputLogin {
  
  func doTapLogin() {
    self.iOutputLogin?.dismissLogin()
    
    }
  
  func doTapSignUp() {
    self.iOutputLogin?.showSignUpNumber()
    ServiceUser.getMovieList()
  }
  
  func doTapForgotPass() {
    
     self.iOutputLogin?.showForgotPass()
  }
}

