//
//  IInputLogin.swift
//  CLocator
//
//  Created by Lucy on 11/27/16.
//  Copyright © 2016 locator. All rights reserved.
//

import Foundation
protocol IInputLogin: NSObjectProtocol {
 
  func doTapSignUp()
  func doTapLogin()
  func doTapForgotPass()
}
