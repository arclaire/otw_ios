//
//  AppDepedencies.swift
//  CLocator
//
//  Created by Lucy on 11/17/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class AppDepedencies {
  
  var wfRoot: WfRoot = WfRoot ()
  init() {
    self.configure()
  }
  
  func installRootVCIntoWindow(window: UIWindow) {
    self.wfRoot.anchorAsRoot(window: window)
  }
  
  func configure() {
  
    let pRoot: PresenterRoot = PresenterRoot()
    pRoot.wfRoot = self.wfRoot
    self.wfRoot.pRoot = pRoot
  }
  
}
