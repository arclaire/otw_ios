//
//  ExtView.swift
//  trining_swift
//
//  Created by Lucy on 4/24/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation

import UIKit

extension UIView {
 
   func loadViewFromNIB() -> UIView {
    let bundle = Bundle(for:type(of: self))
    let nib = UINib(nibName: String(describing: self.classForCoder), bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    
    return view
  }
  
  static func getViewFromNib(bundle bdl: Bundle? = nil, nibName strNibName: String? = nil, tag intTag: Int) -> UIView? {
    let nib: UINib
    
    if let strNibName = strNibName {
      if let bdl = bdl {
        nib = UINib(nibName: strNibName, bundle: bdl)
      } else {
        nib = UINib(nibName: strNibName, bundle: Bundle(for: self.classForCoder()))
      }
    } else {
      if let bdl = bdl {
        nib = UINib(nibName: String(describing: self.classForCoder()), bundle: bdl)
      } else {
        nib = UINib(nibName: String(describing: self.classForCoder()), bundle: Bundle(for: self.classForCoder()))
      }
    }
    
    
    let obj   = nib.instantiate(withOwner: nil, options: nil)
    
    for vw in obj {
      if let vw = vw as? UIView {
        if intTag == -1 {
          if vw.isKind(of: self.classForCoder()) {
            return vw
          }
        } else {
          if vw.tag == intTag {
            return vw
          }
        }
        
      }
    }
    
    return nil
  }
}

extension UIStoryboard {
  static func getMainStoryboard() -> UIStoryboard {
    return UIStoryboard.init(name: "Main", bundle: nil)
  }
}


extension UIFont {
  static func  printAllFonts() {
    for familyName in UIFont.familyNames as [String] {
      print("\(familyName)")
      for fontName in UIFont.fontNames(forFamilyName: familyName) as [String] {
        print("\tFont: \(fontName)")
      }
    }
  }
}
