//
//  ApiService.swift
//  CLocator
//
//  Created by Lucy on 1/3/17.
//  Copyright © 2017 locator. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON

typealias resultHendler = (JSON?,Error?)

class ApiService: NSObject {

  func request(httpMethod: Alamofire.HTTPMethod , urlString: String, requestHendler: @escaping (resultHendler) -> Void) {
    var requestResult: (jsonData: JSON?, error: Error?)
    
    Alamofire.request(urlString).responseJSON { response in
      print(response.request!)  // original URL request
      print(response.response!) // ®HTTP URL response
      print(response.data!)     // server data
      print(response.result)   // result of response serialization
      
      switch response.result {
      case .success:
        if let data = response.result.value {
          requestResult.jsonData = JSON(data)
          requestHendler(requestResult)
          requestHendler(requestResult)
        }
      case .failure(let _error):
        requestResult.error = _error
        requestHendler(requestResult)
      }
    }
  }

}
