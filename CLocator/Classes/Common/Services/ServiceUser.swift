//
//  ServiceUser.swift
//  CLocator
//
//  Created by Lucy on 1/3/17.
//  Copyright © 2017 locator. All rights reserved.
//

import Foundation

typealias callBackError = (Error) -> Void

class ServiceUser {
  //MARK: ATTRIBUTES
  
  static let apiClient = ApiService()
  
  static let BASE_URL = "https://movies-v2.api-fetch.website/"
  static let API_MOVIE_PROVIDER = "movies/"
  //MARK: GET -
  
  //MARK: Movie List
  
  typealias callBackMovieData = ([MovieData]) -> Void
  static func getMovieList(intpage: Int? = nil, onSuccess: callBackMovieData? = nil, onError: callBackError? = nil){
    
    var urlString = "\(BASE_URL)\(API_MOVIE_PROVIDER)"
    
    if let _intPage = intpage {
      urlString = "\(urlString)\(_intPage)"
    }
    
    self.apiClient.request(httpMethod: .get, urlString: urlString) { (result) in
      let (jsonData,error) = result
      
      var arrMovieData = [MovieData]()
      
      
      if let _jsonData = jsonData {
        for data in _jsonData.arrayValue {
          arrMovieData.append(MovieData(jsonData: data))
        }
        onSuccess?(arrMovieData)
      }else {
        onError?(error!)
      }
    }
    
  }
  
  //MARK: TV Show List
  
  
  //MARK: - POST

}
