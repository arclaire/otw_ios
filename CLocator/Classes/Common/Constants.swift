//
//  Constants.swift
//  CLocator
//
//  Created by Lucy on 11/16/16.
//  Copyright © 2016 locator. All rights reserved.
//

import Foundation
import UIKit


let COLOR_BLUE_DEFAULT = UIColor(red: 60.0/255.0, green: 160.0/255.0, blue: 254.0/255.0, alpha: 1.0)
let COLOR_GREEN_DEFAULT = UIColor(red: 99.0/255.0, green: 201.0/255.0, blue: 97.0/255.0, alpha: 1.0)
let COLOR_TEXT_GRAY = UIColor(red: 113.0/255.0, green: 113.0/255.0, blue: 113.0/255.0, alpha: 1.0)
let COLOR_TEXT_BLUE = UIColor(red: 74.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)

let COLOR_BG_DEFAULT = UIColor(red: 240.0/255.0, green: 247.0/255.0, blue: 255.0/255.0, alpha: 1.0)

let IMAGE_MENU_NAV_LEFT = UIImage(named: "left_menu_icon")
let IMAGE_MENU_NAV_BACK = UIImage(named: "left_arrow_icon")

let ID_VC_FORGOT_PASS         = "VCForgotPass"
let ID_VC_HOME                = "VCHome"
let ID_VC_LEFT_MENU           = "VCLeftmenu"
let ID_VC_LOGIN               = "VCLogin"
let ID_VC_PROFILE             = "VCProfile"
let ID_VC_SIGN_UP_NUMBER      = "VCSignUpNumber"
let ID_VC_VERIFICATION        = "VCVerification"

let ID_NIB_VW_CUSTOM          = "ViewCustom"

let ID_TAG_VW_ACCKEYBOARD   = 80

let ID_NVC_HOME               = "NVCHome"
let ID_NVC_LOGIN              = "NVCLogin"

let FONT_UBUNTU_REGULAR       = "Ubuntu"
let FONT_UBUNTU_LIGHT         = "Ubuntu-Light"
let FONT_UBUNTU_ITALIC_MEDIUM = "Ubuntu-MediumItalic"
let FONT_UBUNTU_ITALIC_BOLD   = "Ubuntu-BoldItalic"
let FONT_UBUNTU_ITALIC        = "Ubuntu-Italic"
let FONT_UBUNTU_BOLD          = "Ubuntu-Bold"


let FONT_UBUNTU_TITLE_BOLD              = UIFont(name: FONT_UBUNTU_BOLD, size: 27)
let FONT_UBUNTU_TXT_REGULAR             = UIFont(name: FONT_UBUNTU_REGULAR, size: 16)
let FONT_UBUNTU_PHONE_LIGHT             = UIFont(name: FONT_UBUNTU_LIGHT, size: 14)
let FONT_UBUNTU_TXT_LIGHT               = UIFont(name: FONT_UBUNTU_LIGHT, size: 15)
let FONT_UBUNTU_TXT_CAPTION_LIGHT       = UIFont(name: FONT_UBUNTU_LIGHT, size: 16)
let FONT_UBUNTU_BUTTON_BOLD             = UIFont(name: FONT_UBUNTU_BOLD, size: 16)
let FONT_UBUNTU_NAV_BOLD                = UIFont(name: FONT_UBUNTU_BOLD, size: 17)
