//
//  ViewHeaderProfile.swift
//  CLocator
//
//  Created by Lucy on 12/4/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class ViewHeaderProfile: UIView {

  @IBOutlet weak private var imgProfile: UIImageView!
  @IBOutlet weak private var lblName: UILabel!
  @IBOutlet weak private var lblPhone: UILabel!
   
  @IBOutlet weak private var vwLine: UIView!
  
  private var vwHeaderProfile: UIView!
  
  
  //MARK: Outlets
  
  
  //MARK: Attributes
  weak var delHomeView: DelegateViewHome?
  
  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwHeaderProfile = self.loadViewFromNIB()
    if let _ = self.vwHeaderProfile {
      self.addSubview(self.vwHeaderProfile)
    }
  }
  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    self.prepareUI()
  }

  func prepareUI() {
    self.vwHeaderProfile.backgroundColor = COLOR_BG_DEFAULT
    self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width / 2
    self.imgProfile.layer.borderColor = UIColor.white.cgColor
    self.imgProfile.layer.borderWidth = 2.0
    
    self.lblPhone.font = FONT_UBUNTU_PHONE_LIGHT
    self.lblName.font = FONT_UBUNTU_TXT_REGULAR
    self.lblName.textColor = COLOR_BLUE_DEFAULT
    self.lblPhone.textColor = UIColor.lightGray
    
    let strName: String? = UserDefaults.standard.object(forKey: "name") as? String
    let strPhone: String? = UserDefaults.standard.object(forKey: "phone") as? String
    
    self.lblName.text = strName
    self.lblPhone.text = strPhone
    self.lblName.backgroundColor = UIColor.clear
    self.lblPhone.backgroundColor = UIColor.clear
  }
  
  

}
