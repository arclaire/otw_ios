//
//  UITextFieldWithInset.swift
//  Closet
//
//  Created by Lucy on 4/3/16.
//  Copyright © 2016 Reebonz. All rights reserved.
//

import UIKit

class UITextFieldWithInset: UITextField {

  var insetTextfield:UIEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0) {
    didSet {
     
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  // placeholder position
  override func textRect(forBounds bounds: CGRect) -> CGRect {
    return newBounds(bounds: bounds)
  }
  
  // text position
  override func editingRect(forBounds bounds: CGRect) -> CGRect {
    return newBounds(bounds: bounds)
  }
  
  override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
    return newBounds(bounds: bounds)
  }
  
  private func newBounds(bounds: CGRect) -> CGRect {
    
    var newBounds = bounds
    newBounds.origin.x += self.insetTextfield.left
    newBounds.origin.y += self.insetTextfield.top
    newBounds.size.height -= self.insetTextfield.top + self.insetTextfield.bottom
    newBounds.size.width -= self.insetTextfield.left + self.insetTextfield.right
    return newBounds
  }

}
