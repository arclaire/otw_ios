//
//  ViewAccessoriesKeyboard.swift
//  CLocator
//
//  Created by Lucy on 12/31/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

protocol DelegateViewAccessoriesKeyboard: NSObjectProtocol {
  func delegateTapBtnDone()
  
  
}
class ViewAccessoriesKeyboard: UIView {

  @IBOutlet weak var btnDone: UIBarButtonItem!
  @IBOutlet weak var vwToolbar: UIToolbar!
  
  weak var delViewAccKeyboard: DelegateViewAccessoriesKeyboard?
  
   
  @IBAction func doTapDone(_ sender: Any) {
    self.delViewAccKeyboard?.delegateTapBtnDone()
  }
}
