//
//  RootNB.swift
//  trining_swift
//
//  Created by Lucy on 3/28/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

protocol DelegateRootNB: NSObjectProtocol {
  func delegateToggleLeft()
  func delegateToggleRight()
}


class RootNB: UINavigationBar {
  
  var view:UIView!
  weak var delegateNB: DelegateRootNB?
  
  @IBOutlet weak private var btnLeft: UIButton!
  @IBOutlet weak private var btnRight: UIButton!
  @IBOutlet weak private var lblTitle: UILabel!
  @IBOutlet weak private var vwLineBot: UIView!
  @IBOutlet weak private var imgLeft: UIImageView!
  @IBOutlet weak private var imgRight: UIImageView!
  
  @IBOutlet weak private var consVwLineBotHeight: NSLayoutConstraint!
  
  var nvcParent: UINavigationController? {
    didSet {
      self.nvcParent?.interactivePopGestureRecognizer?.addTarget(self, action:#selector(self.handlePopGesture))
      self.nvcParent?.delegate = self

    }
  }
  
  override var isHidden: Bool {
    didSet {
      if self.isHidden {
        self.setStatusBar(floatAlpha: 0)
      } else {
        self.setStatusBar(floatAlpha: 1)
      }
    }
  }
  var isAnimateHideBar: Bool = false
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.prepareUI()
  }

  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    self.prepareUI()
    
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.backItem?.title = "" // added to hide back title that often spawn
  }
  
  func prepareUI() {
    self.view = self.loadViewFromNib()
    self.view.frame = bounds
    //debugPrint(self.view)
    self.addSubview(view)
    self.view.backgroundColor = UIColor.white
    self.vwLineBot.backgroundColor = UIColor.lightGray
    self.consVwLineBotHeight.constant = 0.5
    
    self.lblTitle.font = FONT_UBUNTU_NAV_BOLD
    self.lblTitle.textColor = COLOR_BLUE_DEFAULT
    self.lblTitle.backgroundColor = UIColor.clear
    
    self.layer.shadowColor = UIColor.gray.cgColor
    self.layer.shadowOffset = CGSize(width: 3.0, height: 1.0)
    
    self.layer.shadowRadius = 1
    self.layer.shadowOpacity = 0.2
    //self.vwContainerInput.layer.shadowPath = UIBezierPath(rect: self.vwContainerInput.bounds).cgPath
    self.layer.shouldRasterize = true
    
  }
  
  func setAnimateAlpha(floatAlpha: CGFloat) {
    if floatAlpha == 1 {
      if self.view.alpha !=  1 {
        
        UIView.animate(withDuration: 0.15, animations: { () -> Void in
          self.view.alpha = 1
          self.setStatusBar(floatAlpha: 1)
        }, completion: { (isComplete) -> Void in
          
        })
      }
    } else {
      self.view.alpha = floatAlpha
      self.setStatusBar(floatAlpha: floatAlpha)
    }
    
  }
  
  func setStatusBar(floatAlpha: CGFloat) {
    let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
    if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
      statusBar.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: floatAlpha)
      
    }
  }
  
  func prepareUINavDefaultWithTitle(str: String) {
    //self.btnLeft.setImage(IMAGE_MENU_NAV_LEFT, for: UIControlState.normal)
    self.imgLeft.image = IMAGE_MENU_NAV_LEFT
    self.lblTitle.text = str
  }
  
  func prepareUINavBackWithTitle(str: String) {
    //self.btnLeft.setImage(IMAGE_MENU_NAV_LEFT, for: UIControlState.normal)jklj
    self.imgLeft.image = IMAGE_MENU_NAV_BACK
    
    self.btnRight.isHidden = true
    self.btnLeft.isHidden =  false
    
    self.imgRight.isHidden = true
    //self.imgLeft.isHidden = true
    self.lblTitle.text = str
    self.backIndicatorImage = UIImage()//nil//IMAGE_MENU_NAV_BACK
    
    /*to hide the back image*/
    self.backIndicatorTransitionMaskImage = UIImage()//nil//IMAGE_MENU_NAV_BACK
    self.backItem?.backBarButtonItem?.title = " "
    
    self.setAnimateAlpha(floatAlpha: 1)
  
  }
  
  func loadViewFromNib() -> UIView {
    let bundle = Bundle(for:type(of: self))
    let nib = UINib(nibName: "RootNB", bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    
    return view
  }

  override func draw(_ rect: CGRect) {
   
  }
  
  override func sizeThatFits(_ size: CGSize) -> CGSize {
    let newSize:CGSize =  CGSize(width: self.superview!.frame.size.width, height: 44) //CGSize(self.superview!.frame.size.width, 87)
    return newSize
  }
  
  //Mark: - Action Methods
  @IBAction func doTapBtnLeft(sender: AnyObject) {
    
    self.delegateNB?.delegateToggleLeft()
  }
  
  @IBAction func doTapBtnRight(_ sender: Any) {
    self.delegateNB?.delegateToggleRight()
  }
  
  func handlePopGesture(gesture: UIGestureRecognizer) {
    let point: CGPoint = gesture.location(in: self)
    let vc: UIViewController = (self.nvcParent?.topViewController)!
   
    if vc.isKind(of: VCLogin.classForCoder()) {
      if gesture.state == UIGestureRecognizerState.began {
        //debugPrint("GESTURE BEGAN", gesture.location(in: self))
        
      } else if gesture.state == UIGestureRecognizerState.changed {
        //debugPrint("GESTURE CHANGED", gesture.location(in: self))
        
        let floatPosX: CGFloat = self.frame.size.width - point.x
        let floatAlpha: CGFloat = floatPosX / self.frame.size.width
        self.setAnimateAlpha(floatAlpha: floatAlpha)
        
      } else if gesture.state == UIGestureRecognizerState.ended{
        
        //debugPrint("GESTURE ENDED", gesture.location(in: self))
      }
    }
   
  }
}

extension RootNB: UINavigationControllerDelegate {
  
  
  func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    if let coordinator = navigationController.topViewController?.transitionCoordinator {
      coordinator.notifyWhenInteractionEnds({ (context) in
        if context.isCancelled {
          self.setAnimateAlpha(floatAlpha: 1)
        } else {
          
          if viewController.isKind(of: VCLogin.classForCoder()){
            self.setAnimateAlpha(floatAlpha: 0)
          }
        }
      })
    } else {
      
    }
  }
}
