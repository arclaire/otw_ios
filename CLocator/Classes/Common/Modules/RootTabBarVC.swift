//
//  RootTabBarVC.swift
//  trining_swift
//
//  Created by Lucy on 3/24/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

class RootTabBarVC: UITabBarController {
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.delegate = self
    
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated
  }
}

extension RootTabBarVC: UITabBarControllerDelegate {
  func tabBarController(_ shouldSelecttabBarController: UITabBarController,shouldSelect viewController: UIViewController) -> Bool {
    return true
  }
}
