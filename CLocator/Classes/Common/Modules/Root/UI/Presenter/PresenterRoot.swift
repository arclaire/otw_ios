//
//  PresenterRoot.swift
//  CLocator
//
//  Created by Lucy on 11/18/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class PresenterRoot: NSObject {

  var wfRoot: WfRoot?
  
  var iRoot: IRoot?
  var iInputRoot: IInputRoot?
  var iOutputRoot: IOutputRoot?
  
}

extension PresenterRoot: MIRoot {
  
  func presentLogin() {
    self.wfRoot?.presentLogin()
  }
  
}

extension PresenterRoot: IOutputRoot {
  
  
}
