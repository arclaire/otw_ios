//
//  WfRoot.swift
//  CLocator
//
//  Created by Lucy on 11/17/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

let idVcRoot = "VCRoot"
class WfRoot: NSObject {

  var wfLeftMenu: WfLeftmenu?
  var wfHome: WfHome?
  var wfLogin: WfLogin?
  var vcHome: VCHome?
    
  var vcRoot: VCRoot = VCRoot() {
    didSet {
      
    }
  }
  
  var isLogin: Bool = false
  
  var nvcHome: UINavigationController?
  var pRoot: PresenterRoot?
  
  func anchorAsRoot(window: UIWindow) {
    let viewController: VCRoot = self.vcRootFromStoryboard()
    
    self.vcRoot = viewController
    self.vcRoot.eventHandler = self.pRoot
    window.rootViewController = self.vcRoot
    self.addMiddleVC()

    self.presentLogin()
  }
  
  func vcRootFromStoryboard() -> VCRoot {
    let storyboard = UIStoryboard.getMainStoryboard()
    let viewController = storyboard.instantiateViewController(withIdentifier: idVcRoot) as! VCRoot
    
    return viewController
  }
  
  func navigationControllerFromWindow(window: UIWindow) -> UIViewController {
    let navigationController: UIViewController = window.rootViewController!
    return navigationController
  }
  
  func addMiddleVC() {
    
    self.wfHome = WfHome()
    self.wfHome?.vcRoot = self.vcRoot
    self.wfHome?.addHomeAsChild()
  }
 
  func presentLogin() {
    self.isLogin = self.vcRoot.isLogin
    
    if !self.isLogin {
      self.wfLogin = WfLogin()
      self.wfLogin?.vcRoot = self.vcRoot
      self.wfLogin?.vcHome = self.wfHome?.vcHome
      self.wfLogin?.nvcRoot = self.wfHome?.nvcHome
      self.wfLogin?.presentLogin()
    }
  }
  
}
