//
//  RootVC.swift
//  CLocator
//
//  Created by Lucy on 11/15/16.
//  Copyright © 2016 locator. All rights reserved.
//
import UIKit



class VCRoot: UIViewController {
  
  @IBOutlet weak var vwParent: UIView!
  
  var vcRoot: VCHome!
  
  var vcRootLeft: VCLeftmenu?
  var ncRoot: RootNC?
  var nbRoot: RootNB?
  
  var eventHandler: MIRoot?
  
  var isLogin: Bool = false
   
  //MARK: - LIFE CYCLE
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.view.backgroundColor = UIColor.white
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    
    UserDefaults.standard.set("Bagus Setiawan", forKey:"name")
    UserDefaults.standard.set("+62812345623", forKey:"phone")
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.eventHandler?.presentLogin()
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

