//
//  IRoot.swift
//  CLocator
//
//  Created by Lucy on 11/18/16.
//  Copyright © 2016 locator. All rights reserved.
//

import UIKit

class IRoot: NSObject {
  weak var iInputRoot: IInputRoot?
  weak var iOutputRoot: IOutputRoot?
}

extension IRoot: IInputRoot {
  
 
}

extension IRoot: IOutputRoot {
  
}
