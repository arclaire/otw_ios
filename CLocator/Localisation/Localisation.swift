//
//  Localisation.swift
//  CLocator
//
//  Created by Lucy on 11/30/16.
//  Copyright © 2016 locator. All rights reserved.
//
import Foundation
let TXT_BASE_LOCATION              = NSLocalizedString("base location", comment: "")
let TXT_DASHBOARD                  = NSLocalizedString("dashboard", comment: "")
let TXT_EMAIL                      = NSLocalizedString("email", comment: "")
let TXT_FAMILY                     = NSLocalizedString("family", comment: "")
let TXT_FRIENDS                    = NSLocalizedString("friends", comment: "")
let TXT_FORGOT_PASSWORD            = NSLocalizedString("forgot password", comment: "")
let TXT_INFO_TITLE_FORGOT_PASSWORD = NSLocalizedString("info title forgot password", comment: "")
let TXT_INFO_TITLE_SIGN_UP_PHONE   = NSLocalizedString("info title sign up phone", comment: "")

let TXT_PASSWORD                   = NSLocalizedString("password", comment: "")
let TXT_LOGIN                      = NSLocalizedString("login", comment: "")
let TXT_SEND                       = NSLocalizedString("send", comment: "")
let TXT_SEND_CODE_AGAIN            = NSLocalizedString("send code again", comment: "")
let TXT_SETTINGS                   = NSLocalizedString("settings", comment: "")
let TXT_SIGN_UP                    = NSLocalizedString("signup", comment: "")
let TXT_PHONE_NUMBER               = NSLocalizedString("phone number", comment: "")
let TXT_PROFILE_INFO               = NSLocalizedString("profile info", comment: "")
